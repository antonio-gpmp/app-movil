#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

import subprocess

process1 = subprocess.Popen(['python3', 'subscriber.py'])
process2 = subprocess.Popen(['python3', 'subscriber-voltage.py'])
process3 = subprocess.Popen(['python3', 'subscriber-ApparentPower.py'])
process1 = subprocess.Popen(['python3', 'subscriber-ActivePower.py'])
process2 = subprocess.Popen(['python3', 'subscriber-PowerFactor.py'])
process3 = subprocess.Popen(['python3', 'subscriber-energy.py'])
