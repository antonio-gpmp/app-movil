#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

import sys
import requests
import json
from libcitisim import Broker

NOTIFY_MSG = '''
New notification:
  data: {:.2f}
  source: {}'''


class TemperatureSubscriber:
    def run(self, args):
        config = 'subscriber-bidir.config'
        if len(args) > 1:
            config = args[1]

        broker = Broker(config)

        # - subscribe only to a specific publisher
        # source = "FFFF735700000002"
        # broker.subscribe_to_publisher(source, self.event_printer)
        # print("Subscribing to '" + source + "' publisher")

        # - subscribe to all publishers of a channel
        topic_name = "Humidity"
        broker.subscribe(topic_name, self.event_printer)
        print("Subscribing to '" + topic_name + "' topic")

        print("Awaiting data...")
         # Hacemos una peticion a orion y recibimos la entidad
        response = requests.get('http://195.220.224.150:1026/version')
        #Transofrmamos la respuesta en un objeto python
        x = json.loads(response.text)
        print(json.dumps(x , indent=4))
        broker.wait_for_events()

    def event_printer(self, value, source, metadata):
        print(NOTIFY_MSG.format(value, source))
        dic = {}
        lista= []
        # lista += ["data: {}".format(value),"source: {}".format(source)]
        dic.update({'data': str(value)})
        dic.update({'source' : source})
        for key in metadata:
            print("  * {}: {}".format(key, metadata[key]))
            # lista += [" {""}:{""} ".format(key, metadata[key])]
            dic.update({key : metadata[key] })

        headers = {'Content-Type': 'application/json'}
        data= '{'+ '"id":'+'"{s}",'.format(s=dic.get('source'))+'"type":"HumiditySensor",'+ '"data":{ ' + '"value":"{d}"'.format(d=dic.get('data'))+','+'"type":"String"'+'},'+'"timestamp":{'+'"value":"{t}"'.format(t=dic.get('timestamp'))+','+'"type":"String"'+'}'+'}'
        data1 = json.dumps(data)
        print(data1)
        data2 = json.loads(data1)
        print(data2)
        url = "http://195.220.224.150:1026/v2/entities"
        response = requests.request("POST", url, data=data2, headers=headers)
        if(response.status_code == 422) :
                data= '{'+ '"data":{ ' + '"value":"{d}"'.format(d=dic.get('data'))+','+'"type":"String"'+'},'+'"timestamp":{'+'"value":"{t}"'.format(t=dic.get('timestamp'))+','+'"type":"String"'+'}'+'}'
                data3= json.dumps(data)
                data4= json.loads(data3)
                url = 'http://195.220.224.150:1026/v2/entities/{id}/attrs'.format(id=dic.get('source'))
                res = requests.request("PATCH", url, data=data4, headers=headers)
                print("Actualizacion de la entidad")
                print(res.status_code)
        print("\n")
        print(dic)
        print(response)
        print(response.text)

if __name__ == "__main__":
    TemperatureSubscriber().run(sys.argv)
