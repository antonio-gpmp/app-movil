import { Component, OnInit, ViewChild } from '@angular/core';
import { PeticionesService } from '../../services/peticiones.service';
import { ActivatedRoute, Router } from '@angular/router';

import { Chart } from 'chart.js';
import { async } from '@angular/core/testing';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.page.html',
  styleUrls: ['./estadisticas.page.scss'],
})
export class EstadisticasPage implements OnInit {
  @ViewChild('barChart',  { static: true }) barChart;
  bars: any;
  colorArray: any;

  titulo;
  valoresCadena;
  sensor;
  dataSensorResp = [];
  dataSensorTarjeta = [];

  dataHistoric = [];
  timeHistoric = [];

  temp_data;
  temp_dataId;
  dataSet = [];

  // tslint:disable-next-line: max-line-length
  constructor( private infoPeticion: PeticionesService, private route: ActivatedRoute, public alertCtrl: AlertController, private rout: Router ) {
      // Para sacar el parametro de la url
      this.titulo = window.location.pathname;
      this.valoresCadena = String(this.titulo).split('/');
      this.sensor = this.valoresCadena[2];
   }

  ngOnInit() {

    switch (this.sensor) {
      case 'Temperatura':
        this.infoPeticion.getInfoEntitisTemperature()
            .subscribe( (resp: any[]) => {
              if (resp.length != 0 ) {
                this.dataSensorResp.push(...resp);
              } else {
                this.presentAlert();
              }
          });
        this.infoPeticion.getHistoricEntitiTemperature()
                  .subscribe( (resp: any[]) => {
                          this.procesRequHistoric(resp);
          });
        break;

      case 'Energia':
         this.infoPeticion.getInfoEntitisEnergia()
            .subscribe( (resp: any[]) => {
              if (resp.length != 0 ) {
                this.dataSensorResp.push(...resp);
              } else {
                this.presentAlert();
              }
          });
         this.infoPeticion.getHistoricEntitiEnergia()
                  .subscribe( (resp: any[]) => {
                      if (resp.length != 0) {
                          this.procesRequHistoric(resp);
                      }
          });
         break;

      case 'Voltaje':
        this.infoPeticion.getInfoEntitisVoltaje()
            .subscribe( (resp: any[]) => {
              if (resp.length != 0 ) {
                this.dataSensorResp.push(...resp);
              } else {
                this.presentAlert();
              }
          });
        this.infoPeticion.getHistoricEntitiVoltaje()
            .subscribe( (resp: any[]) => {
              if (resp.length != 0) {
                  this.procesRequHistoric(resp);
              }
          });
        break;

      case 'Ozono':
        this.infoPeticion.getInfoEntitisOzono()
        .subscribe( (resp: any[]) => {
          if (resp.length != 0 ) {
            this.dataSensorResp.push(...resp);
          } else {
            this.presentAlert();
          }
      });
        this.infoPeticion.getHistoricEntitiOzono()
          .subscribe( (resp: any[]) => {
            if (resp.length != 0) {
                this.procesRequHistoric(resp);
            }
        });
        break;
      case 'Humedad':
        this.infoPeticion.getInfoEntitisHumedad()
        .subscribe( (resp: any[]) => {
          if (resp.length != 0 ) {
            this.dataSensorResp.push(...resp);
          } else {
            this.presentAlert();
          }
      });
        this.infoPeticion.getHistoricEntitiHumedad()
          .subscribe( (resp: any[]) => {
            if (resp.length != 0) {
                this.procesRequHistoric(resp);
            }
        });
        break;
      case 'Power':
        this.infoPeticion.getInfoEntitisPower()
        .subscribe( (resp: any[]) => {
          if (resp.length != 0 ) {
            this.dataSensorResp.push(...resp);
          } else {
            this.presentAlert();
          }
      });
        this.infoPeticion.getHistoricEntitiPower()
          .subscribe( (resp: any[]) => {
            if (resp.length != 0) {
                this.procesRequHistoric(resp);
            }
        });
        break;

    }
  }

  procesRequHistoric(resp) {
    console.log('RESP ', resp);
    this.temp_data = resp.entities.map(resp => resp.values);
    this.temp_dataId = resp.entities.map(resp => resp.entityId);
    console.log('Temp Data: ', ...this.temp_data);
    console.log('Ids ', this.temp_dataId);
    this.createLinesGrap(this.temp_data);
  }


  ionViewDidEnter() {
  }
  numeroaleatorio() {
    return Math.floor((Math.random() * 300) + 1);
  }
  async createLinesGrap( temp_data ) {
    // tslint:disable-next-line: prefer-for-of
    for (let index = 0; index < temp_data.length; index++) {
      this.dataSet.push(
        {
          label: this.temp_dataId[index],
          data: temp_data[index],
          backgroundColor: 'rgb(0, 0, 0, 0)',
          borderColor: 'rgb(' + this.numeroaleatorio() + ',' + this.numeroaleatorio() + ',' + this.numeroaleatorio() + ')',
          borderWidth: 2
        });

      this.createBarChart();
    }
  }

  createBarChart() {
    this.bars = new Chart(this.barChart.nativeElement, {
      type: 'line',
      data: {
        labels: this.dataSet[0].data,
        datasets: [
          ...this.dataSet
        ],
      },
      options: {
        legend: {
          display: true
        },
        responsive: true,
        scales: {
          xAxes: [{
            ticks: {
                beginAtZero: true,
                stacked: true,
                display: false

            }
          }]
        }
      }
    });

  }

  async presentAlert() {
  const alert = await this.alertCtrl.create({
    header:  'Sensor no disponible',
    subHeader: '',
    message: 'Por favor, consulte otro sensor',
    buttons: [
      {
        text: 'Cancelar',
        handler : () => {
        }
      }, {
        text: 'Aceptar',
        handler : () => {
          this.rout.navigate(['tabs/tab2/']);
        }
      }
    ]
 });

  await alert.present();

}

}

// [26.00, 30.00, 27.00, 27.00, 28.00, 21, 35.00, 35.00, 34.00, 34.00, 33.00, 33.00, 32.00, 32.00, 31.00, 31.00, 25.00, 35.00, 20.00, 40],
// [{x: 20},{x: 30},{x: 25},{x: 40},{x: 35},{x: 30},{x: 25},{x: 20}],

// {
//   label: 'Valores en el mes',
//   data: [26.00, 30.00, 27.00, 27.00, 28.00, 21, 35.00, 35.00, 34.00, 34.00, 33.00, 33.00, 32.00, 32.00, 31.00, 31.00, 25.00, 35.00, 20.00, 40],
//   backgroundColor: 'rgb(0, 0, 0, 0)',
//   borderColor: 'rgb(' + this.numeroaleatorio() + ',' + this.numeroaleatorio() + ',' + this.numeroaleatorio() + ')',
//   borderWidth: 2
// }

// Let’s go over some of the parameters and their usage
// type — Type defines the variety of chart e.g. line, bar, pie etc
// data — Data is the dataset which you are plotting on the charts.
// data → labels — To label a particular data set
// data → datasets — data object for one dataset. Multiple such objects can be plotted together
// scales — scales contains options for X and Y axes, grid options, sizing etc.

