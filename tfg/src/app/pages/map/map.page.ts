import { Component, OnInit } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { ActivatedRoute } from '@angular/router';
import { PeticionesService } from '../../services/peticiones.service';
import { AlertController } from '@ionic/angular';

declare var mapboxgl: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  titulo;
  valoresCadena;
  sensor;

  colecLatit;
  colecLongit;
  lat = 0;
  lng = 0;

  map;

  constructor( private route: ActivatedRoute, private infoPeticion: PeticionesService, public alertCtrl: AlertController) {

    this.titulo = window.location.pathname;
    this.valoresCadena = String(this.titulo).split('/');
    this.sensor = this.valoresCadena[2];
   }

  ngOnInit() {
    switch (this.sensor) {
      case 'Temperatura':
        this.infoPeticion.getInfoEntitisTemperature()
            .subscribe( (resp: any[]) => {
                  this.rellenarLtLong(resp);
          },
          error => {
            this.presentAlert();
        });
        break;

      case 'Energia':
        this.infoPeticion.getInfoEntitisEnergia()
            .subscribe( (resp: any[]) => {
                  this.rellenarLtLong(resp);
          },
          error => {
            this.presentAlert();
        });
        break;

      case 'Voltaje':
        this.infoPeticion.getInfoEntitisVoltaje()
            .subscribe( (resp: any[]) => {
                  this.rellenarLtLong(resp);
          },
          error => {
            this.presentAlert();
        });
        break;

      case 'Ozono':
        this.infoPeticion.getInfoEntitisOzono()
            .subscribe( (resp: any[]) => {
                  this.rellenarLtLong(resp);
          },
          error => {
            this.presentAlert();
        });
        break;
      case 'Humedad':
        this.infoPeticion.getInfoEntitisHumedad()
            .subscribe( (resp: any[]) => {
                  this.rellenarLtLong(resp);
          },
          error => {
            this.presentAlert();
        });
        break;
      case 'Power':
        this.infoPeticion.getInfoEntitisPower()
            .subscribe( (resp: any[]) => {
                  this.rellenarLtLong(resp);
          },
          error => {
            this.presentAlert();
        });
        break;

    }



    mapboxgl.accessToken = 'pk.eyJ1IjoiYW50b25pby1ncG1wIiwiYSI6ImNrN3RiNzduejA1MHMzZ3Ayajl4NHc2b3UifQ.fkMKyd_U-gP8NVXShywM1Q';

  }

  rellenarLtLong(resp) {
     this.colecLatit =  resp.map(resp => resp.latitude.value);
     this.colecLongit = resp.map(resp => resp.longitude.value);

      this.loadMap();
  }


  loadMap() {

    if(this.colecLongit[0] && this.colecLatit[0]){
     this.map = new mapboxgl.Map({
      style: 'mapbox://styles/mapbox/outdoors-v10',
      center: [ this.colecLongit[0] , this.colecLatit[0] ],   // longitud , latitud  38.9902389,-3.9222622,17   -74.0066, 40.7135
      zoom: 15.5,
      pitch: 45,
      bearing: -17.6,
      container: 'map',
      antialias: true
    });

    

  } else {

     this.map = new mapboxgl.Map({
      style: 'mapbox://styles/mapbox/outdoors-v10',
      center: [ -3.9222221842689197 , 38.990293049070246 ],   // longitud , latitud  38.9902389,-3.9222622,17   -74.0066, 40.7135
      zoom: 11,
      pitch: 45,
      bearing: -17.6,
      container: 'map',
      antialias: true
    });

  }

  this.map.on('load', () => {

    this.map.resize();

    // tslint:disable-next-line: prefer-for-of
    for (let index = 0; index < this.colecLongit.length; index++) {


      new mapboxgl.Marker()
        .setLngLat([ this.colecLongit[index] , this.colecLatit[index] ])
        .addTo(this.map);
    }

      // Insert the layer beneath any symbol layer.
    const layers = this.map.getStyle().layers;

    let labelLayerId;
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < layers.length; i++) {
          if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
            labelLayerId = layers[i].id;
            break;
          }
        }

    this.map.addLayer( {
        id: '3d-buildings',
        source: 'composite',
        'source-layer': 'building',
        filter: ['==', 'extrude', 'true'],
        type: 'fill-extrusion',
        minzoom: 15,
        paint: {
        'fill-extrusion-color': '#aaa',

      // use an 'interpolate' expression to add a smooth transition effect to the
      // buildings as the user zooms in
      'fill-extrusion-height': [
          'interpolate',
          ['linear'],
          ['zoom'],
          15,
          0,
          15.05,
          ['get', 'height']
          ],
          'fill-extrusion-base': [
          'interpolate',
          ['linear'],
          ['zoom'],
          15,
          0,
          15.05,
          ['get', 'min_height']
          ],
          'fill-extrusion-opacity': 0.6
          }
        },

      labelLayerId
      );
  });  

  }


  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header:  'Sensor fuera de servicio',
      subHeader: '',
      message: 'Por favor, consulte otro sensor',
      buttons: [
        {
          text: 'OK',
          handler : () => {
            //this.rout.navigate(['tabs/tab2/']);
          }
        }
      ]
   });
    await alert.present();
  }




}
