import { Component } from '@angular/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  sensores = [
    {
      titulo: 'Energia',
      foto: '/assets/Energia-2.svg',
      clase:'colEnergia',
      redirectTo:'/tab-est-map'
    },
    {
      titulo: 'Voltaje',
      foto: '/assets/Voltaje.svg',
      clase:'colVoltage',
      redirectTo:'/tab-est-map'
    },
    {
      titulo: 'Ozono',
      foto: '/assets/Ozono.svg',
      clase:'colOzono',
      redirectTo:'/tab-est-map'
    },
    {
      titulo: 'Humedad',
      foto: '/assets/humidity.svg',
      clase:'colHumedad',
      redirectTo:'/tab-est-map'
    },
    {
      titulo: 'Temperatura',
      foto: '/assets/Temperature-1.svg',
      clase:'colTemperatura',
      redirectTo:'/tab-est-map'
    },
    {
      titulo: 'Power',
      foto: '/assets/battery-1.svg',
      clase:'colPower',
      redirectTo:'/tab-est-map'
    }
  ];

  constructor() {}

}
