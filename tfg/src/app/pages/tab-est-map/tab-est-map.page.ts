import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tab-est-map',
  templateUrl: './tab-est-map.page.html',
  styleUrls: ['./tab-est-map.page.scss'],
})
export class TabEstMapPage implements OnInit, AfterContentInit {

  titulo;

  constructor( private rout: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    this.titulo = this.route.snapshot.paramMap.get('titulo');
  }

  ngAfterContentInit() {
    // Para que al cargar el componente reedirija directametne a una opción + el parametro de los sensores
    this.rout.navigate(['tab-est-map/' + this.titulo + '/estadisticas']);

    console.log('Valor ' + this.titulo);

  }

}
