import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabEstMapPage } from './tab-est-map.page';

const routes: Routes = [
  {
    path: '',
    component: TabEstMapPage,
    children: [
      {
        path: 'map',
        loadChildren: '../map/map.module#MapPageModule'
      },
      {
        path: 'estadisticas',
        loadChildren: '../estadisticas/estadisticas.module#EstadisticasPageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabEstMapPageRoutingModule {}
