import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabEstMapPageRoutingModule } from './tab-est-map-routing.module';

import { TabEstMapPage } from './tab-est-map.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabEstMapPageRoutingModule
  ],
  declarations: [TabEstMapPage]
})
export class TabEstMapPageModule {}
