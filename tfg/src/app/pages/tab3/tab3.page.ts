import { Component, ViewChild , OnInit} from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { PeticionesService } from '../../services/peticiones.service';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  @ViewChild(IonSegment, {static: true}) segment: IonSegment;

  total: any [] = [];

  temp_data;

  totalTemperature = [];
  maxTem;
  maxTemauxMes;
  minTem;
  minTemauxMes;
  maxTemauxSem;
  minTemauxSem;
  maxTemauxDia;
  minTemauxDia;

  totalEnergia = [];
  maxEng;
  maxEngMes;
  minEng;
  minEngMes;
  maxEngSem;
  minEngSem;
  maxEngDia;
  minEngDia;

  totalVoltaje = [];
  maxVol;
  maxVolMes;
  minVol;
  minVolMes;
  maxVolSem;
  minVolSem;
  maxVolDia;
  minVolDia;

  totalOzone = [];
  maxOzo;
  maxOzoMes;
  minOzo;
  minOzoMes;
  maxOzoSem;
  minOzoSem;
  maxOzoDia;
  minOzoDia;

  totalHumedad = [];
  maxHum;
  maxHumMes;
  minHum;
  minHumMes;
  minHumSem;
  maxHumSem;
  minHumDia;
  maxHumDia;

  totalPower = [];
  maxPow;
  maxPowMes;
  minPow;
  minPowMes;
  minPowSem;
  maxPowSem;
  minPowDia;
  maxPowDia;

  resultTotal = [];

  constructor(private infoPeticion: PeticionesService) {}

  ngOnInit() {
    // Esto hace que cuando se carge la pagina este en el valor de día
    // this.segment.value = 'dia';

    /////////////////////////////// MES /////////////////////////////////
    this.infoPeticion.getHistoricMaxTemperature()
          .subscribe( (resp: any[]) => {
                  this.totalTemperature = this.procesRequHistoric(resp);
                  this.totalTemperature.sort((elem1, elem2) => elem2 - elem1);
                  this.maxTemauxMes = this.totalTemperature[0];
                  console.log('Temp Data MAX: ', this.totalTemperature[0]);
            },
            error => {
              this.maxTemauxMes = 'sin datos';
    });
    this.infoPeticion.getHistoricMinTemperature()
          .subscribe( (resp: any[]) => {
                this.totalTemperature = this.procesRequHistoric(resp);
                this.totalTemperature.sort((elem1, elem2) => elem1 - elem2);
                this.minTemauxMes = this.totalTemperature[0];
                console.log('Temp Data MIN: ', this.totalTemperature[0]);
          },
          error => {
            this.minTemauxMes = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxEnergia()
        .subscribe( (resp: any[]) => {
              this.totalEnergia = this.procesRequHistoric(resp);
              this.totalEnergia.sort((elem1, elem2) => elem2 - elem1);
              this.maxEngMes = this.totalEnergia[0];
        },
        error => {
          this.maxEngMes = 'sin datos';
  });
    this.infoPeticion.getHistoricMinEnergia()
        .subscribe( (resp: any[]) => {
            this.totalEnergia = this.procesRequHistoric(resp);
            this.totalEnergia.sort((elem1, elem2) => elem1 - elem2);
            this.minEngMes = this.totalEnergia[0];
        },
        error => {
          this.minEngMes = 'sin datos';
  });
    this.infoPeticion.getHistoricMaxVoltaje()
        .subscribe( (resp: any[]) => {
            this.totalVoltaje = this.procesRequHistoric(resp);
            this.totalVoltaje.sort((elem1, elem2) => elem2 - elem1);
            this.maxVolMes = this.totalVoltaje[0];
        },
          error => {
            this.maxVolMes = 'sin datos';
    });
    this.infoPeticion.getHistoricMinVoltaje()
        .subscribe( (resp: any[]) => {
            this.totalVoltaje = this.procesRequHistoric(resp);
            this.totalVoltaje.sort((elem1, elem2) => elem1 - elem2);
            this.minVolMes = this.totalVoltaje[0];
            console.log('Temp Data MIN: ', this.totalVoltaje[0]);
        },
          error => {
            this.minVolMes = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxOzono()
        .subscribe( (resp: any[]) => {
            this.totalOzone = this.procesRequHistoric(resp);
            this.totalOzone.sort((elem1, elem2) => elem2 - elem1);
            this.maxOzoMes = this.totalOzone[0];
            console.log('Temp Data MAX: ', this.totalOzone[0]);
        },
          error => {
            this.maxOzoMes = 'sin datos';
    });
    this.infoPeticion.getHistoricMinOzono()
        .subscribe( (resp: any[]) => {
            this.totalOzone = this.procesRequHistoric(resp);
            this.totalOzone.sort((elem1, elem2) => elem1 - elem2);
            this.minOzoMes = this.totalOzone[0];
            console.log('Temp Data MIN: ', this.totalOzone[0]);
      },
      error => {
        this.minOzoMes = 'sin datos';
    });

    this.infoPeticion.getHistoricMaxHumedad()
      .subscribe( (resp: any[]) => {
          this.totalHumedad = this.procesRequHistoric(resp);
          this.totalHumedad.sort((elem1, elem2) => elem2 - elem1);
          this.maxHumMes = this.totalHumedad[0];
          console.log('Temp Data MAX: ', this.totalHumedad[0]);
    },
      error => {
        this.maxHumMes = 'sin datos';
    });

    this.infoPeticion.getHistoricMinHumedad()
      .subscribe( (resp: any[]) => {
          this.totalHumedad = this.procesRequHistoric(resp);
          this.totalHumedad.sort((elem1, elem2) => elem1 - elem2);
          this.minHumMes = this.totalHumedad[0];
          console.log('Temp Data MIN: ', this.totalHumedad[0]);
      },
        error => {
          this.minHumMes = 'sin datos';
    });

    this.infoPeticion.getHistoricMaxPower()
      .subscribe( (resp: any[]) => {
          this.totalPower = this.procesRequHistoric(resp);
          this.totalPower.sort((elem1, elem2) => elem2 - elem1);
          this.maxPowMes = this.totalPower[0];
    },
      error => {
        this.maxPowMes = 'sin datos';
    });

    this.infoPeticion.getHistoricMinPower()
      .subscribe( (resp: any[]) => {
          this.totalPower = this.procesRequHistoric(resp);
          this.totalPower.sort((elem1, elem2) => elem1 - elem2);
          this.minPowMes = this.totalPower[0];
      },
        error => {
          this.minPowMes = 'sin datos';
    });

    //////////////////////////// SEMANA /////////////////////////////////
    this.infoPeticion.getHistoricMaxTemperatureWeek()
        .subscribe( (resp: any[]) => {
                this.totalTemperature = this.procesRequHistoric(resp);
                this.totalTemperature.sort((elem1, elem2) => elem2 - elem1);
                this.maxTemauxSem = this.totalTemperature[0];
          },
          error => {
            this.maxTemauxSem = 'sin datos';
    });
    this.infoPeticion.getHistoricMinTemperatureWeek()
        .subscribe( (resp: any[]) => {
                this.totalTemperature = this.procesRequHistoric(resp);
                this.totalTemperature.sort((elem1, elem2) => elem1 - elem2);
                this.minTemauxSem = this.totalTemperature[0];
          },
          error => {
            this.minTemauxSem = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxEnergiaWeek()
        .subscribe( (resp: any[]) => {
              this.totalEnergia = this.procesRequHistoric(resp);
              this.totalEnergia.sort((elem1, elem2) => elem2 - elem1);
              this.maxEngSem = this.totalEnergia[0];
        },
        error => {
          this.maxEngSem = 'sin datos';
    });
    this.infoPeticion.getHistoricMinEnergiaWeek()
        .subscribe( (resp: any[]) => {
            this.totalEnergia = this.procesRequHistoric(resp);
            this.totalEnergia.sort((elem1, elem2) => elem1 - elem2);
            this.minEngSem = this.totalEnergia[0];
        },
        error => {
          this.minEngSem = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxVoltajeWeek()
        .subscribe( (resp: any[]) => {
            this.totalVoltaje = this.procesRequHistoric(resp);
            this.totalVoltaje.sort((elem1, elem2) => elem2 - elem1);
            this.maxVolSem = this.totalVoltaje[0];
        },
          error => {
            this.maxVolSem = 'sin datos';
    });
    this.infoPeticion.getHistoricMinVoltajeWeek()
        .subscribe( (resp: any[]) => {
            this.totalVoltaje = this.procesRequHistoric(resp);
            this.totalVoltaje.sort((elem1, elem2) => elem1 - elem2);
            this.minVolSem = this.totalVoltaje[0];
            console.log('Temp Data MIN: ', this.totalVoltaje[0]);
        },
          error => {
            this.minVolSem = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxOzonoWeek()
        .subscribe( (resp: any[]) => {
            this.totalOzone = this.procesRequHistoric(resp);
            this.totalOzone.sort((elem1, elem2) => elem2 - elem1);
            this.maxOzoSem = this.totalOzone[0];
        },
          error => {
            this.maxOzoSem = 'sin datos';
    });
    this.infoPeticion.getHistoricMinOzonoWeek()
        .subscribe( (resp: any[]) => {
            this.totalOzone = this.procesRequHistoric(resp);
            this.totalOzone.sort((elem1, elem2) => elem1 - elem2);
            this.minOzoSem = this.totalOzone[0];
      },
      error => {
        this.minOzoSem = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxHumedadWeek()
      .subscribe( (resp: any[]) => {
          this.totalHumedad = this.procesRequHistoric(resp);
          this.totalHumedad.sort((elem1, elem2) => elem2 - elem1);
          this.maxHumSem = this.totalHumedad[0];
    },
      error => {
        this.maxHumSem = 'sin datos';
    });
    this.infoPeticion.getHistoricMinHumedadWeek()
      .subscribe( (resp: any[]) => {
          this.totalHumedad = this.procesRequHistoric(resp);
          this.totalHumedad.sort((elem1, elem2) => elem1 - elem2);
          this.minHumSem = this.totalHumedad[0];
      },
        error => {
          this.minHumSem = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxPowerWeek()
      .subscribe( (resp: any[]) => {
          this.totalPower = this.procesRequHistoric(resp);
          this.totalPower.sort((elem1, elem2) => elem2 - elem1);
          this.maxPowSem = this.totalPower[0];
    },
      error => {
        this.maxPowSem = 'sin datos';
    });

    this.infoPeticion.getHistoricMinPowerWeek()
      .subscribe( (resp: any[]) => {
          this.totalPower = this.procesRequHistoric(resp);
          this.totalPower.sort((elem1, elem2) => elem1 - elem2);
          this.minPowSem = this.totalPower[0];
      },
        error => {
          this.minPowSem = 'sin datos';
    });

    //////////////////////////// DIA /////////////////////////////////////////////////
    this.infoPeticion.getHistoricMaxTemperatureDay()
          .subscribe( (resp: any[]) => {
                  this.totalTemperature = this.procesRequHistoric(resp);
                  this.totalTemperature.sort((elem1, elem2) => elem2 - elem1);
                  this.maxTemauxDia = this.totalTemperature[0];
                  console.log('Temp Data MAX: ', this.totalTemperature[0]);
            },
            error => {
              this.maxTemauxDia = 'sin datos';
    });
    this.infoPeticion.getHistoricMinTemperatureDay()
          .subscribe( (resp: any[]) => {
                this.totalTemperature = this.procesRequHistoric(resp);
                this.totalTemperature.sort((elem1, elem2) => elem1 - elem2);
                this.minTemauxDia = this.totalTemperature[0];
                console.log('Temp Data MIN: ', this.totalTemperature[0]);
          },
          error => {
            this.minTemauxDia = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxEnergiaDay()
        .subscribe( (resp: any[]) => {
              this.totalEnergia = this.procesRequHistoric(resp);
              this.totalEnergia.sort((elem1, elem2) => elem2 - elem1);
              this.maxEngDia = this.totalEnergia[0];
        },
        error => {
          this.maxEngDia = 'sin datos';
    });
    this.infoPeticion.getHistoricMinEnergiaDay()
        .subscribe( (resp: any[]) => {
            this.totalEnergia = this.procesRequHistoric(resp);
            this.totalEnergia.sort((elem1, elem2) => elem1 - elem2);
            this.minEngDia = this.totalEnergia[0];
        },
        error => {
          this.minEngDia = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxVoltajeDay()
        .subscribe( (resp: any[]) => {
            this.totalVoltaje = this.procesRequHistoric(resp);
            this.totalVoltaje.sort((elem1, elem2) => elem2 - elem1);
            this.maxVolDia = this.totalVoltaje[0];
        },
          error => {
            this.maxVolDia = 'sin datos';
    });
    this.infoPeticion.getHistoricMinVoltajeDay()
        .subscribe( (resp: any[]) => {
            this.totalVoltaje = this.procesRequHistoric(resp);
            this.totalVoltaje.sort((elem1, elem2) => elem1 - elem2);
            this.minVolDia = this.totalVoltaje[0];
        },
          error => {
            this.minVolDia = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxOzonoDay()
        .subscribe( (resp: any[]) => {
            this.totalOzone = this.procesRequHistoric(resp);
            this.totalOzone.sort((elem1, elem2) => elem2 - elem1);
            this.maxOzoDia = this.totalOzone[0];
        },
          error => {
            this.maxOzoDia = 'sin datos';
    });
    this.infoPeticion.getHistoricMinOzonoDay()
        .subscribe( (resp: any[]) => {
            this.totalOzone = this.procesRequHistoric(resp);
            this.totalOzone.sort((elem1, elem2) => elem1 - elem2);
            this.minOzoDia = this.totalOzone[0];
      },
      error => {
        this.minOzoDia = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxHumedadDay()
      .subscribe( (resp: any[]) => {
          this.totalHumedad = this.procesRequHistoric(resp);
          this.totalHumedad.sort((elem1, elem2) => elem2 - elem1);
          this.maxHumDia = this.totalHumedad[0];
    },
      error => {
        this.maxHumDia = 'sin datos';
    });
    this.infoPeticion.getHistoricMinHumedadDay()
      .subscribe( (resp: any[]) => {
          this.totalHumedad = this.procesRequHistoric(resp);
          this.totalHumedad.sort((elem1, elem2) => elem1 - elem2);
          this.minHumDia = this.totalHumedad[0];
      },
        error => {
          this.minHumDia = 'sin datos';
    });
    this.infoPeticion.getHistoricMaxPowerDay()
      .subscribe( (resp: any[]) => {
          this.totalPower = this.procesRequHistoric(resp);
          this.totalPower.sort((elem1, elem2) => elem2 - elem1);
          this.maxPowDia = this.totalPower[0];
    },
      error => {
        this.maxPowDia = 'sin datos';
    });

    this.infoPeticion.getHistoricMinPowerDay()
      .subscribe( (resp: any[]) => {
          this.totalPower = this.procesRequHistoric(resp);
          this.totalPower.sort((elem1, elem2) => elem1 - elem2);
          this.minPowDia = this.totalPower[0];      
      },
        error => {
          this.minPowDia = 'sin datos';
    });

    setTimeout(() => { this.segment.value = 'dia'; }, 2000);
   

  }


  Dia() {
    
    this.maxTem = this.maxTemauxDia;
    this.minTem = this.minTemauxDia;

    this.maxEng = this.maxEngDia;
    this.minEng = this.minEngDia;

    this.maxVol = this.maxVolDia;
    this.minVol = this.minVolDia;

    this.maxOzo = this.maxOzoDia;
    this.minOzo = this.minOzoDia;

    this.maxHum = this.maxHumDia;
    this.minHum = this.minHumDia;

    this.maxPow = this.maxPowDia;
    this.minPow = this.minPowDia;

  }
  Semana() {
    this.maxTem = this.maxTemauxSem;
    this.minTem = this.minTemauxSem;

    this.maxEng = this.maxEngSem;
    this.minEng = this.minEngSem;

    this.maxVol = this.maxVolSem;
    this.minVol = this.minVolSem;

    this.maxOzo = this.maxOzoSem;
    this.minOzo = this.minOzoSem;

    this.maxHum = this.maxHumSem;
    this.minHum = this.minHumSem;

    this.maxPow = this.maxPowSem;
    this.minPow = this.minPowSem;

  }
  Mes() {
    this.maxTem = this.maxTemauxMes;
    this.minTem = this.minTemauxMes;

    this.maxEng = this.maxEngMes;
    this.minEng = this.minEngMes;

    this.maxVol = this.maxVolMes;
    this.minVol = this.minVolMes;

    this.maxOzo = this.maxOzoMes;
    this.minOzo = this.minOzoMes;

    this.maxHum = this.maxHumMes;
    this.minHum = this.minHumMes;

    this.maxPow = this.maxPowMes;
    this.minPow = this.minPowMes;

  }




  segmentChanged(event) {
    const valorSegmento = event.detail.value;
    console.log(valorSegmento);
  }

  cambioFecha( event) {
    console.log('ionChange', event);
    console.log('FechaEvento', new Date(event.detail.value));
  }


procesRequHistoric(resp) {
    this.temp_data = resp.entities.map(resp => resp.values);
    let aux = [];
    // tslint:disable-next-line: prefer-for-of
    for (let index = 0; index < this.temp_data.length; index++) {
      aux = aux.concat(this.temp_data[index]);
    }
    return aux;
  }

}