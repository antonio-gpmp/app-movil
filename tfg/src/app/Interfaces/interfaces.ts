interface GetInfoEntity {
  attributes: Attribute[];
  entityId: string;
  index: string[];
}

interface Attribute {
  attrName: string;
  values: string[];
}


interface TemperatureSensor {
  id: string;
  type: string;
  data: Data;
  expiration: Data;
  latitude: Data;
  longitude: Data;
  place: Data;
  quality: Data;
  timestamp: Data;
}

interface Data {
  type: string;
  value: string;
  metadata: Metadata;
}

interface Metadata {
  
}


interface TemperatureHistoric {
  attrName: string;
  entities: Entity[];
  entityType: string;
}

interface Entity {
  entityId: string;
  index: string[];
  values: string[];
}