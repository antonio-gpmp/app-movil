import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PeticionesService {
  date = new Date();
  primerDiaMes;
  ultimoDiaMes;

  curr = new Date;
  firstdayWeek;
  lastdayWeek;

  // tslint:disable-next-line: new-parens
  hoy = new Date;
  fechaHoy;
  fechaMania;

  constructor(private http: HttpClient) {
    // Sacar el mes
    this.primerDiaMes = new Date(this.date.getFullYear(), this.date.getMonth(), 2).toISOString();
    this.ultimoDiaMes = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).toISOString();
    this.primerDiaMes = this.primerDiaMes.substr(0, 10);
    this.ultimoDiaMes = this.ultimoDiaMes.substr(0, 10);
    // Sacar la semana
    this.firstdayWeek = new Date(this.curr.setDate(this.curr.getDate() - this.curr.getDay() + 1)).toISOString();
    this.lastdayWeek = new Date(this.curr.setDate(this.curr.getDate() - this.curr.getDay() + 7)).toISOString();
    this.firstdayWeek = this.firstdayWeek.substr(0, 10);
    this.lastdayWeek = this.lastdayWeek.substr(0, 10);
    // Sacar el días
    this.fechaHoy = this.hoy.getFullYear() + '-' + (this.hoy.getMonth() + 1) + '-' + this.hoy.getDate();
    this.fechaMania = this.hoy.getFullYear() + '-' + (this.hoy.getMonth() + 1) + '-' + (this.hoy.getDate() + 1);
    
  }


  getInfoEntity() {
     return this.http.get<GetInfoEntity>(`http://195.220.224.150:8668/v2/entities/0A03000000008378`);
  }
  // PETICIONES A ORION
  // Crear interfaz --> cmd+shif+ P y seleccionar la opcion JSON to Clipboard

  // TEMPERATURA
  getInfoEntitisTemperature() {
    return this.http.get(`http://195.220.224.150:1026/v2/entities?type=TemperatureSensor`);
  }
  getHistoricEntitiTemperature(){
    // http://195.220.224.150:8668/v2/entities/0A06175100000010?type=TemperatureSensor
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/TemperatureSensor/attrs/data?fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxTemperature(){
    // http://195.220.224.150:8668/v2/entities/0A06175100000010?type=TemperatureSensor
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/TemperatureSensor/attrs/data?aggrMethod=max&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMinTemperature(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/TemperatureSensor/attrs/data?aggrMethod=min&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxTemperatureWeek(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/TemperatureSensor/attrs/data?aggrMethod=max&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMinTemperatureWeek(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/TemperatureSensor/attrs/data?aggrMethod=min&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMaxTemperatureDay(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/TemperatureSensor/attrs/data?aggrMethod=max&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }
  getHistoricMinTemperatureDay(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/TemperatureSensor/attrs/data?aggrMethod=min&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }

  // ENERGIA
  getInfoEntitisEnergia() {
    return this.http.get(`http://195.220.224.150:1026/v2/entities?type=EnergySensor`);
  }
  getHistoricEntitiEnergia(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/EnergySensor/attrs/data?fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxEnergia(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/EnergySensor/attrs/data?aggrMethod=max&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMinEnergia(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/EnergySensor/attrs/data?aggrMethod=min&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxEnergiaWeek(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/EnergySensor/attrs/data?aggrMethod=max&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMinEnergiaWeek(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/EnergySensor/attrs/data?aggrMethod=min&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMaxEnergiaDay(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/EnergySensor/attrs/data?aggrMethod=max&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }
  getHistoricMinEnergiaDay(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/EnergySensor/attrs/data?aggrMethod=min&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }



  // VOLTAJE
  getInfoEntitisVoltaje() {
    return this.http.get(`http://195.220.224.150:1026/v2/entities?type=VoltageSensor`);
  }
  getHistoricEntitiVoltaje(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/VoltageSensor/attrs/data?fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxVoltaje(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/VoltageSensor/attrs/data?aggrMethod=max&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMinVoltaje(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/VoltageSensor/attrs/data?aggrMethod=min&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxVoltajeWeek(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/VoltageSensor/attrs/data?aggrMethod=max&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMinVoltajeWeek(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/VoltageSensor/attrs/data?aggrMethod=min&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMaxVoltajeDay(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/VoltageSensor/attrs/data?aggrMethod=max&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }
  getHistoricMinVoltajeDay(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/VoltageSensor/attrs/data?aggrMethod=min&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }


  // OZONO
  getInfoEntitisOzono() {
    return this.http.get(`http://195.220.224.150:1026/v2/entities?type=OzoneSensor`);
  }
  getHistoricEntitiOzono(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/OzoneSensor/attrs/data?fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxOzono(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/OzoneSensor/attrs/data?aggrMethod=max&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMinOzono(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/OzoneSensor/attrs/data?aggrMethod=min&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxOzonoWeek(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/OzoneSensor/attrs/data?aggrMethod=max&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMinOzonoWeek(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/OzoneSensor/attrs/data?aggrMethod=min&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMaxOzonoDay(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/OzoneSensor/attrs/data?aggrMethod=max&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }
  getHistoricMinOzonoDay(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/OzoneSensor/attrs/data?aggrMethod=min&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }


  // HUMEDAD
  getInfoEntitisHumedad() {
    return this.http.get(`http://195.220.224.150:1026/v2/entities?type=HumiditySensor`);
  }
  getHistoricEntitiHumedad(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/HumiditySensor/attrs/data?fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxHumedad(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/HumiditySensor/attrs/data?aggrMethod=max&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMinHumedad(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/HumiditySensor/attrs/data?aggrMethod=min&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxHumedadWeek(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/HumiditySensor/attrs/data?aggrMethod=max&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMinHumedadWeek(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/HumiditySensor/attrs/data?aggrMethod=min&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMaxHumedadDay(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/HumiditySensor/attrs/data?aggrMethod=max&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }
  getHistoricMinHumedadDay(){
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/HumiditySensor/attrs/data?aggrMethod=min&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }


  // POWER
  getInfoEntitisPower() {
    return this.http.get(`http://195.220.224.150:1026/v2/entities?type=PowerSensor`);
  }
  getHistoricEntitiPower() {
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/PowerSensor/attrs/data?fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxPower() {
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/PowerSensor/attrs/data?aggrMethod=max&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMinPower() {
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/PowerSensor/attrs/data?aggrMethod=min&fromDate=` + this.primerDiaMes + `&toDate=` + this.ultimoDiaMes);
  }
  getHistoricMaxPowerWeek() {
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/PowerSensor/attrs/data?aggrMethod=max&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMinPowerWeek() {
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/PowerSensor/attrs/data?aggrMethod=min&fromDate=` + this.firstdayWeek + `&toDate=` + this.lastdayWeek);
  }
  getHistoricMaxPowerDay() {
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/PowerSensor/attrs/data?aggrMethod=max&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }
  getHistoricMinPowerDay() {
    // tslint:disable-next-line: max-line-length
    return this.http.get(`http://195.220.224.150:8668/v2/types/PowerSensor/attrs/data?aggrMethod=min&fromDate=` + this.fechaHoy + `&toDate=` + this.fechaMania);
  }
}
